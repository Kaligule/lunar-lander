.PHONY : clean

# $^ is an automatic variable that expands to the name of all the prerequisites,
# with spaces between them.
# https://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html
public: play.html play.css game.js engine.js documentation.html landing_page.html landing_page.css
	rm -rf public
	# put everything into the public folder
	mkdir public
	mv $^ public/
	cp --update --recursive static public/
	# rename the landing_page to index
	mv public/landing_page.html public/index.html

# tangle
play.html play.css game.js engine.js landing_page.html landing_page.css &: lunarlander.org
	emacs -Q --batch \
              --eval "(require 'org)" \
              --eval "(org-babel-do-load-languages 'org-babel-load-languages '((python . t)))" \
              --eval '(setq org-confirm-babel-evaluate nil)' \
              --eval '(org-babel-tangle-file "lunarlander.org")'

# weave (export)
documentation.html: lunarlander.org export_helpers/htmlize/htmlize.el
	emacs -Q --batch \
              --eval "(require 'org)" \
              --eval '(setq org-confirm-babel-evaluate nil)' \
              --eval "(org-babel-do-load-languages 'org-babel-load-languages '((python . t)))" \
              --load 'export_helpers/htmlize/htmlize.el' \
              lunarlander.org \
              --funcall org-html-export-to-html
	mv lunarlander.html documentation.html

# needed for colorizing source blocks in html export
export_helpers/htmlize/htmlize.el:
	git clone --quiet "https://github.com/hniksic/emacs-htmlize.git" export_helpers/htmlize/

clean:
	rm play.html play.css game.js engine.js documentation.html landing_page.html landing_page.css
	rm -r public
